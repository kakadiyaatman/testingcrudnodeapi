const express = require("express");
const app = express();
const helmet = require('helmet');
const { sequelize } = require("./models");
require('dotenv').config();

app.use(
  express.urlencoded({
    limit: "30mb",
    extended: true,
  })
);
app.use(
  express.json({
    limit: "30mb",
    extended: true,
  })
);
app.use(helmet());

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "*");
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

// routes
app.use("/user", require("./routes/users/app"));
app.use(async (req, res) => {
  res.status(404).send('Route is no where to be found.');
})

const port = process.env.PORT || 5000;

app.listen(port, async () => {
  console.log(`server is running on port : ${port}`);
  await sequelize.sync();
  console.log('database synced!')
});
