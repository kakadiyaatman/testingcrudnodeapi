const userLogin = ({ queries, hashings, geneToken }) => {
    return async (req, res) => {
        try {
            const { email, password } = req.body;
            const checkUser = await queries.findByEmail(email);
            if (checkUser) {
                const isPasswordCorrect = await hashings.decryptPass(password, checkUser.password);
                if (!isPasswordCorrect) {
                    res.status(404).json({
                        status: 'error',
                        message: 'Invalid password!'
                    });
                } else {
                    const token = await geneToken(checkUser.email, process.env.JWT_PRIVATE_KEY);
                    res.status(200).json({
                        status: "success",
                        message: 'Login Successful!',
                        name: checkUser.name,
                        email: checkUser.email,
                        token: token
                    });
                }
            } else {
                return res.status(404).json({
                    status: 'error',
                    message: 'email not found'
                });
            }
        } catch (e) {
            console.log(e.message);
        }
    }
};

module.exports = userLogin;