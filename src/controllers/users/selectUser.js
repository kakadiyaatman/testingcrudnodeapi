const getUserDetails = ({ queries }) => {
    return async (req, res) => {
        const email = req.email;
        const data = await queries.findUserDetails(email);
        if (data) {
            res.status(200).json({
                status: "success",
                data: data,
            });
        } else {
            res.status(404).json({
                status: "error",
                message: 'record not found',
            });
        };
    };
};

module.exports = getUserDetails;