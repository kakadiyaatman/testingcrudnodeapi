const updateUserDetails = ({ queries, imgRemove }) => {
    return async (req, res) => {
        let dataRecord = { };
        const auemail = req.email;
        const { name } = req.body;
        const checkUser = await queries.findByEmail(auemail);
        if (checkUser) {
            const pathToFile = process.env.ATTACHMENT_PATH + checkUser.avimg;
            if (req.file) {
                dataRecord = {
                    name,
                    avimg: req.file.filename,
                    updated_date: new Date()
                }
            } else {
                dataRecord = {
                    name,
                    updated_date: new Date()
                }
            }
            await queries.updateUser(dataRecord, auemail).then(() => {
                res.status(200).json({
                    status: "success",
                    message: 'data updated successfully',
                });
                if (req.file) {
                    imgRemove(pathToFile);
                }
            }).catch((e) => {
                res.status(400).json({
                    status: "error",
                    message: e.message,
                });
            });
        } else {
            res.status(404).json({
                status: "error",
                message: 'user not find',
            });
        }
    };
};

module.exports = updateUserDetails;