const deleteUser = ({ queries, imgRemove }) => {
    return async (req, res) => {
        const email = req.email;
        const checkUser = await queries.findByEmail(email);
        if (checkUser) {
            queries.deleteUserDetails(email).then(() => {
                const pathToFile = process.env.ATTACHMENT_PATH + checkUser.avimg;
                res.status(200).json({
                    status: "success",
                    message: 'user deleted successfully'
                });
                imgRemove(pathToFile);
            }).catch((e) => {
                res.status(400).json({
                    status: "error",
                    message: e.message,
                });
            });
        }
        else {
            return res.status(404).json({
                status: 'error',
                message: 'user not found'
            });
        }
    };
};

module.exports = deleteUser;