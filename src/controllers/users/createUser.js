const userSignUp = ({ queries, hashings }) => {
    return async (req, res) => {
        const { name, email, password } = req.body;
        const hashpassword = await hashings.encryptPass(password);
        console.log(hashpassword)
        try {
            const checkUser = await queries.findByEmail(email);
            if (checkUser) {
                res.status(404).json({
                    status: "error",
                    data: 'user already exist with email',
                });
            } else {
                const temp = queries.createUser({
                    name,
                    email,
                    password: hashpassword,
                    avimg: req.file.filename,
                }).then((data) => {
                    res.status(201).json({
                        status: "success",
                        message: 'user created successfully',
                        data
                    });
                }).catch((e) => {
                    res.status(400).json({
                        status: "error",
                        message: e.message,
                    });
                });
            }
        } catch (e) {
            console.log(e.message);
        }
    };
}

module.exports = userSignUp;