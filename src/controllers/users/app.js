const queries = require('../../services/users/app');
const { hashings, imgRemove, geneToken } = require('../../functions/app');

const usersAdd = require('./createUser');
const usersSelect = require('./selectUser');
const usersUpdate = require('./updateUser');
const usersDelete = require('./deleteUser');
const usersLogin = require('./loginUser');

const userAdd = usersAdd({ queries, hashings });
const userSelect = usersSelect({ queries });
const userUpdate = usersUpdate({ queries, imgRemove });
const userDelete = usersDelete({ queries, imgRemove });
const userLogin = usersLogin({ queries, hashings, geneToken });

const services = Object.freeze({
    userAdd,
    userSelect,
    userUpdate,
    userDelete,
    userLogin,
});

module.exports = services;

module.exports = {
    userAdd,
    userSelect,
    userUpdate,
    userDelete,
    userLogin,
};
