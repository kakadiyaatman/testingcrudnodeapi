const multer = require('multer');
const jwt = require('jsonwebtoken');

const authUsers = require('./auth');
const uploads = require('./avimgUpload');

const upload = uploads({ multer });
const authUser = authUsers({ jwt });

const services = Object.freeze({
    upload,
    authUser
});

module.exports = services;
module.exports = {
    upload,
    authUser
}