const uploads = ({ multer }) => {
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, process.env.ATTACHMENT_PATH);
        },
        filename: function (req, file, cb) {
            var ext = file.mimetype.split('/')[1];
            cb(null, Date.now() + "." + ext);
        }
    })
    const upload = multer({ storage: storage });
    return upload;
}

module.exports = uploads;