const authUsers = ({ jwt }) => {
    return async (req, res, next) => {
        // token 
        if (req.headers.authorization) {
            const token = await req.headers.authorization.split(' ')[1];
            jwt.verify(token, process.env.JWT_PRIVATE_KEY, async (err, decoded) => {
                if (decoded) {
                    req.email = decoded.email;
                    next();
                } else if (err) {
                    return res.status(401).json({
                        status: 'error',
                        message: 'invalid token!'
                    });
                }
            });
        } else {
            return res.status(401).json({
                status: 'error',
                message: 'Unauthorized'
            });
        }
    }
};
module.exports = authUsers;