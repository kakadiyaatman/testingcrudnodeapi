const imgRemoves = ({ fs }) => {
    return async (imgPath) => {
        fs.unlink(imgPath, function (err) {
            if (err) {
                throw err
            } else {
                console.log("Successfully deleted the file.");
            }
        });
    }
}

module.exports = imgRemoves;