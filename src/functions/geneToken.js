const geneTokens = ({ jwt }) => {
    return async (mail, key) => {
        return await jwt.sign({ email: mail }, key);
    }
};

module.exports = geneTokens;
