const hashing = ({ bcryptjs }) => {
    return Object.freeze({
        encryptPass,
        decryptPass,
    });
    async function encryptPass(password) {
        try {
            return await bcryptjs.hash(password, 12);
        }
        catch (e) {
            console.log(e.message);
        }
    };
    async function decryptPass(pass, encPass) {
        try {
            return await bcryptjs.compare(pass, encPass);
        }
        catch (e) {
            console.log(e.message);
        }
    }
};

module.exports = hashing;