const bcryptjs = require('bcryptjs');
const fs = require("fs");
const jwt = require('jsonwebtoken');


const hashing = require('./hashing');
const imgRemoves = require('./imgRemove');
const geneTokens = require('./geneToken');

const hashings = hashing({ bcryptjs });
const imgRemove = imgRemoves({ fs });
const geneToken = geneTokens({ jwt });

const services = Object.freeze({
    imgRemove,
    geneToken
});

module.exports = services;

module.exports = { hashings, imgRemove, geneToken };