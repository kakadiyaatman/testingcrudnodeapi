const query = ({ User }) => {
    return Object.freeze({
        findByEmail, findUserDetails, createUser, updateUser, deleteUserDetails
    });

    async function findByEmail(mail) {
        try {
            return await User.findOne({ where: { email: mail } });
        }
        catch (e) {
            console.log(e.message);
        }
    };

    async function findUserDetails(mail) {
        try {
            return await User.findOne({ where: { email: mail }, attributes: { exclude: ['password'] } });
        }
        catch (e) {
            console.log(e.message);
        }
    };

    async function createUser(query) {
        try {
            return await User.create(query);
        }
        catch (e) {
            console.log(e.message);
        }
    };

    async function updateUser(data, mail) {
        try {
            return await User.update(data, { where: { email: mail } });
        }
        catch (e) {
            console.log(e.message);
        }
    }
    async function deleteUserDetails(mail) {
        try {
            return await User.destroy({ where: { email: mail } });
        }
        catch (e) {
            console.log(e.message);
        }
    };
};

module.exports = query;