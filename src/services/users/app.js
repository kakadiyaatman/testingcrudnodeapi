const { User } = require("../../models");

const query = require('./query');

const queries = query({ User });

module.exports = queries;