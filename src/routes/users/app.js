const express = require("express");
const router = express.Router();

const route = require("./routes");

const { upload, authUser } = require('../../middleware/app');
//#########
const routes = route({ router, authUser, upload });

const services = Object.freeze({
    routes,
});

module.exports = services;

module.exports = {
    routes,
};
module.exports = router;