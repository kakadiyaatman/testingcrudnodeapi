// const {
// userSignUp, userLogin, getUserDetails, updateUserDetails, deleteUser } = require("../../controllers/user");

const { userAdd, userSelect, userUpdate, userDelete, userLogin, } = require('../../controllers/users/app');

const routes = ({ router, authUser, upload }) => {
    router.post("/userSignUp", upload.single('attachment'), userAdd);

    router.post("/userLogin", userLogin);

    router.post("/getUserDetails", authUser, userSelect);

    router.patch("/updateUserDetails", authUser, upload.single('attachment'), userUpdate);

    router.post("/deleteUser", authUser, userDelete);

    return router;
}

module.exports = routes;
